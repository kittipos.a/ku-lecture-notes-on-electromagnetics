
|[บทที่ 1](ch1TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |  

|[ก่อนหน้า](ch1-05-1TH.md)| [ต่อไป](ch1-06TH.md) |
| ---------- | ---------- |    

## 1.5.2 [การคูณแบบครอส (cross product)](ch1-05-2TH.md)  
การคูณแบบครอส ของเวกเตอร์ $`\mathbf a`$ กับ $`\mathbf b`$ มีสมการดังนี้  
```math
\mathbf a \times \mathbf b = \mathbf a_n |\mathbf a| |\mathbf b| \sin \theta  
```  
เมื่อ $`\mathbf a_n`$ เป็นเวกเตอร์หนึ่งหน่วยที่ตั้งฉากกับเวกเตอร์ $`\mathbf a`$ และ $`\mathbf b`$ ดังรูปด้านล่าง  
<img src="https://upload.wikimedia.org/wikipedia/commons/d/d2/Right_hand_rule_cross_product.svg" width="100" >  
และมุม $`\theta`$ เป็นมุมระหว่างเวกเตอร์ $`\mathbf a`$ กับ $`\mathbf b`$ ดังรูปด้านล่าง  
<img src="https://upload.wikimedia.org/wikipedia/commons/b/b0/Cross_product_vector.svg" width="100" >   
จากรูปจะเห็นว่า $`\mathbf a \times \mathbf b = -\mathbf b \times \mathbf a`$ หรือมีทิศทางตรงข้ามกัน  
$`\mathbf a \times \mathbf b`$ อ่านว่า $`\mathbf a`$ ครอส $`\mathbf b`$ โดยเวกเตอร์หนึ่งหน่วยในพิกัดฉาก $`\mathbf a_x, \mathbf a_y, \mathbf a_z`$ มีความสัมพันธ์กันดังนี้
```math 
\mathbf a_x \times \mathbf a_y = \mathbf a_z \newline
\mathbf a_y \times \mathbf a_z = \mathbf a_x \newline
\mathbf a_z \times \mathbf a_x = \mathbf a_y \newline
```  
ถ้าให้ $`\mathbf A = A_x \mathbf a_x+ A_y \mathbf a_y+ A_z \mathbf a_z`$ และ $`\mathbf B = B_x \mathbf a_x+ B_y \mathbf a_y+ B_z \mathbf a_z`$ เราจะหา $`\mathbf A \times \mathbf B`$ โดยการกระจายแต่ละส่วนประกอบของเวกเตอร์เข้าไปทำการคูณแบบครอสทีละตัวซึ่งจะได้ผลลัพธ์ดังนี้  
```math 
\mathbf A \times \mathbf B = \newline 
(A_y B_z - A_z B_y) \mathbf a_x + (A_z B_x - A_x B_z) \mathbf a_y + (A_x B_y - A_y B_x) \mathbf a_z
```  
ตัวอย่างการหาการคูณแบบครอสของเวกเตอร์ดังรูปด้านล่าง (ดัดแปลงจาก [Hayt 2011](#Hayt))   
<img src="./asset/dot1.svg" width="300" >  
จากรูป $`\mathbf r_p =  \mathbf a_x+ 2 \mathbf a_y+ 3 \mathbf a_z`$ และ $`\mathbf r_q = 2 \mathbf a_x- \mathbf a_y+  \mathbf a_z`$ และ[ตัวอย่างในหัวข้อการคูณแบบดอท](ch1-05-1TH.md#ตัวอย่าง)  จะได้ว่า $`|\mathbf r_P| = \sqrt{14}`$ และ $`|\mathbf r_Q| = \sqrt{x_Q^2+y_Q^2+z_Q^2} = \sqrt{6}`$ และ $`\theta = 71^o`$ ดังนั้น 
```math
|\mathbf r_p \times \mathbf r_q| =   |\mathbf r_p| |\mathbf r_q| \sin \theta  \newline
|\mathbf r_p \times \mathbf r_q| =   \sqrt{14}\sqrt{6} \sin 71^o  \newline
```
เราสามารถหาค่า$`\sqrt{14}\sqrt{6} \sin 71^o`$ จากเว็บไซต์ [octave-online](https://octave-online.net/) โดยพิมพ์คำสั่งตามด้านล่างซึ่งจะได้คำตอบเท่ากับ 8.67  
```
octave:1> sqrt(14)*sqrt(6)*sin(71*pi/180)
ans = 8.6658
```  
หาการคูณแบบครอสตามสามาการจะได้  
```math
\mathbf r_p \times \mathbf r_q = \newline 
(r_{p_y} r_{q_z} - r_{p_z} r_{q_y}) \mathbf a_x + (r_{p_z} r_{q_x} - r_{p_x} r_{q_z}) \mathbf a_y + (r_{p_x} r_{q_y} - r_{p_y} r_{q_x}) \mathbf a_z \newline 

(2 \cdot 1 - 3 \cdot (-1)) \mathbf a_x +  (3 \cdot 2 - 1 \cdot 1) \mathbf a_y + (1 \cdot (-1) - 2 \cdot 2) \mathbf a_z \newline

5 \mathbf a_x +  5 \mathbf a_y -5 \mathbf a_z \newline

5 (\mathbf a_x +   \mathbf a_y - \mathbf a_z) \newline
5 \sqrt 3 \cdot \frac { \mathbf a_x +   \mathbf a_y - \mathbf a_z}{\sqrt 3} \newline
```
ขนาดของ $`\mathbf r_p \times \mathbf r_q`$ คือ $`5 \sqrt 3 = 8.66`$ และทิศทาง $`\mathbf a_n = \frac { \mathbf a_x +   \mathbf a_y - \mathbf a_z}{\sqrt 3} `$  
เราสามารถหาค่า $`\mathbf r_p \times \mathbf r_q`$ จากเว็บไซต์ [octave-online](https://octave-online.net/) โดยพิมพ์คำสั่งตามด้านล่าง  
```
octave:1> rp = [1 2 3]
rp =

   1   2   3

octave:2> rq = [2 -1 1]
rq =

   2  -1   1

octave:3> crossprod = cross(rp,rq)
crossprod =

   5   5  -5

octave:4> mag_crossprod = sqrt(dot(crossprod,crossprod))
mag_crossprod = 8.6603
```  

รายละเอียดเพิ่มเติมสามารถดูได้ที่   
- [ocw.mit.edu](https://ocw.mit.edu/courses/mathematics/18-02sc-multivariable-calculus-fall-2010/1.-vectors-and-matrices/part-a-vectors-determinants-and-planes/session-7-cross-products)  
- <a name="Hayt"></a> Hayt, W. H., & Buck, J. A. (2011). Engineering Electromagnetics (8th ed.). McGraw-Hill Professional.




|[ก่อนหน้า](ch1-05-1TH.md)| [ต่อไป](ch1-06TH.md) |
| ---------- | ---------- |    
