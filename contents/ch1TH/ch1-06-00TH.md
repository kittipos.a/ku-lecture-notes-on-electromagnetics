
[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  


|[บทที่ 1](ch1TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |    

|[ก่อนหน้า](ch1-06TH.md)| [ต่อไป](ch1-07TH.md) |
| ---------- | ---------- |    

### การแปลงชื่อของจุดระหว่างพิกัดฉาก $`(x,y,z)`$   กับพิกัดแบบทรงกระบอก $`(\rho,\phi,z)`$  
ในรูป $`(d)`$ เราจะเห็นว่า  
- $`x = \rho \cos \phi`$  
- $`y = \rho \sin \phi`$  
- $`\rho = \sqrt{x^2+y^2}`$  
- $`\phi =\tan^{-1} \frac {y}{x}`$  

ค่า $`\rho`$ มีค่ามากว่าหรือเท่าศูนย์เสมอเพราะเป็นระยะห่างจากแกน $`z`$ และค่า $`\phi`$ มีค่าตั้งแต่ศุนย์ถึง $`2 \pi`$ เรเดียน ที่แกน $`x`$ ค่า $`\phi=0`$ ที่แกน $`y`$ ค่า $`\phi=\frac{\pi}{2}`$ ที่แกน $`-x`$ ค่า $`\phi=\pi`$ ที่แกน $`-y`$ ค่า $`\phi=\frac{2 \pi}{2}`$ จะวนกลับไปที่แกน $`x`$ โดยจุดที่เกือบถึงแกน $`x`$ จะมีค่าเกือบถึง $`2 \pi`$ แต่จะไม่ถึง $`2 \pi`$ เพราะจะไปซ้ำกับจุดบนแกน $`x`$ ซึ่งกำหนดไปแล้วว่า $`\phi=0`$ โดยปกติ $`\tan^{-1}`$ จะมีค่าอยู่ในช่วง $`-\frac{\pi}{2}`$ ถึง $`\frac{\pi}{2}`$ ดังนั้นเวลากดเครื่องคิดเลขเราจะต้องนำค่าทีได้มาแปลงเป็นค่าที่อยู่ในช่วงศูนย์ถึง $`2\pi`$ ตามนิยามของ $`\phi`$ เช่น ถ้า $`x=-1`$ และ $`y=-1`$ เรากดเครื่องคิดเลขโดยพิมพ์ $`tan^{-1}\frac{-1}{-1}`$ เราจะได้คำตอบเป็น $`\frac{\pi}{4}`$ แต่จริงๆแล้วจุดอยู่ในควอแดรนต์ที่สาม $`\phi`$ มีค่าเท่ากับ $`\pi \frac{\pi}{4} = \frac{5\pi}{4}`$   
ในระบบหน่วย SI หน่วยของ $`\phi`$ คือ เรเดียน ถ้าเราไม่ได้เขียนหน่วย เราจะคือว่าหน่วยที่ใช้เป็นหน่วย SI ถ้าเราต้องการใช้หน่วยเป็นองศา เราต้องเขียนหน่วยกำกับไว้เสมอ  

ที่จุด $`P(x=1,y=2,z=3)`$ เราจะได้ว่า 
- $`1 = \rho \cos \phi`$  
- $`2 = \rho \sin \phi`$  
หรือ $`\rho = \sqrt{ 1^2 + 2^2} = \sqrt{ 5}`$ และ $`\phi = \tan \frac{2}{1} = 1.1071`$ ดังนั้นจุด $`P(x=1,y=2,z=3)`$ จะมีชื่อว่า $`P(\rho=\sqrt{5},\phi=1.1071,z=3)`$ ในพิกัดแบบทรงกระบอก  

### ตัวอย่างการประยุกต์ใช้พิกัดแบบทรงกระบอก  
1.6.1 [ตัวอย่างการหาความยาวของเส้นโค้ง](ch1-06-01TH.md)  
1.6.2 [ตัวอย่างการหาพื้นที่ครึ่งวงกลม](ch1-06-02TH.md)  
1.6.3 [ตัวอย่างการหาปริมาตรครึ่งทรงกลม](ch1-06-03TH.md)  

|[ก่อนหน้า](ch1-06TH.md)| [ต่อไป](ch1-07TH.md) |
| ---------- | ---------- |    

<table align="center">
<tr><td align="center" width="9999">
<img src="./asset/cylin_ab.svg" align="center" width=50% alt="" />
</td></tr>
</table>  

(a) แสดงพื้นผิวที่ตัวแปรในพิกัดแบบทรงกระบอกมีค่าคงที่ (b) แสดงทิศทางของแกน $`\rho`$ แกน $`\phi`$ และ แกน $`z`$ ที่จุด P (ดัดแปลงจาก [Hayt 2011](#Hayt))  

<table align="center">
<tr><td align="center" width="9999">
<img src="./asset/cylin_c.svg" align="center" width=50% alt="" />
</td></tr>
</table>  

(c) แสดงความยาวดิฟเฟอเรนเชียล (differential length) ปริมาตร ดิฟเฟอเรนเชียล (differential volume) พื้นผิวดิฟเฟอเรนเชียล (differential surface) (ดัดแปลงจาก [Hayt 2011](#Hayt))

<table align="center">
<tr><td align="center" width="9999">
<img src="./asset/cyline_cartesian.svg" align="center" width=50% alt="" />
</td></tr>
</table>  

(d) แสดงการแปลงชื่อของจุดระหว่างพิกัดฉากกับพิกัดแบบทรงกระบอก (differential surface) (ดัดแปลงจาก [Hayt 2011](#Hayt))

อ้างอิง  
- <a name="Hayt"></a> Hayt, W. H., & Buck, J. A. (2011). Engineering Electromagnetics (8th ed.). McGraw-Hill Professional.

|[ก่อนหน้า](ch1-06TH.md)| [ต่อไป](ch1-07TH.md) |
| ---------- | ---------- |    


[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  


